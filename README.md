# **OGMO Plugin for Magento 2**

## Table of contents
* [General info](#general-info)
* [Setup](#setup)

## General info
OGMO, powered by LiveRoom, is a service provided for Magento eCommerce vendors. This extension will adding new button "Live View" near to "Add to Cart" button in all the products which are linked with OGMO. This plugin gives viewer through the plugin which enables visualization of 3D or AR (AR-enabled device required) representation of your product without making the user navigate to another website.
## Setup

### **Install via composer (recommend)**

1. Open your terminal (CMD/SSH) and go to your Magento 2 site root directory
2. Configure repository and Install OGMO plugin

     ```
    composer config repositories.ogmo-test git https://gitlab.com/Team_LiveRoom/ogmo-plugin-magento-2.git
    composer require liveroom/module-ogmo:<version(tag)>
     ```
3. Enable module and setup Upgrade

   ```
   php -f bin/magento module:enable --clear-static-content Liveroom_Ogmo
   php -f bin/magento setup:upgrade
   ```

4. Deploy static content

   `php bin\magento setup:static-content:deploy -f`


### **Install manually (recommended for v0.5.0)**

1. Connect to your instance with SFTP/SSH and upload ​Liveroom_Ogmo module to the <​Magento root>/app/code folder (It is better if you upload zip module and extract it within the magento site).
   - If your instance is bitnami, your root folder is /home/bitnami/apps/magento/htdocs/
   - If your instance is ubuntu, your root folder is /var/www/html/

2. Go to the Magento root folder and run following commands.

   Setup Upgrade Using Command Line - ```sudo php bin/magento setup:upgrade```
   Cache clear using command Line - ```sudo php bin/magento cache:flush```
   Reindexing Using Command Line - ```sudo php bin/magento indexer:reindex```
   Deploy static content - ```sudo php bin/magento setup:static-content:deploy -f```
   
3. Go to the Magento root folder and run following command to check the module status.
   ```sudo php bin/magento module:status```

4. After the installation when you are loading the online store and if there is an error, go to the Magento root folder, give permissions to the following folders using the following commands.
   ```sudo chmod -R 777 var/cache/```
   ```sudo chmod -R 777 pub/static/```
   ```sudo chmod -R 777 vendor/```
   ```sudo chmod -R 777 generated/```

5. Go to the root folder and run the following command.
   ```sudo php bin/magento setup:di:compile```
