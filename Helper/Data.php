<?php

namespace ogmo_ar\ar_product_visualizer\Helper;
use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
       protected $zendClient;
       
       public function __construct(\Zend\Http\Client $zendClient)
       {
              $this->zendClient = $zendClient;
       }

       public function getlink($sku)
       {
              # base_url value will change when building
              $base_url = 'https://api.app2.ogmo.xyz/magento/plugin?';
              $paraArray = array(
                     'sku' => $sku,
                     'host' => $_SERVER['HTTP_HOST']
                            );

              $para = http_build_query($paraArray);
              $feed_url = $base_url.$para;

              try 
              {
                     $this->zendClient->reset();
                     $this->zendClient->setUri($feed_url);
                     $this->zendClient->setMethod(\Zend\Http\Request::METHOD_GET);
                     $this->zendClient->setOptions(array(
                            'timeout' => 10
                     ));
                     
                     $this->zendClient->send();
                     $response = $this->zendClient->getResponse();

                     $tmp = preg_split('/^\r?$/m', $response, 2);
                     $dat = trim($tmp[1]);

                     $obj = json_decode($dat);

                     if($obj != null)
                     {
                            $state = $obj->status;
                            
                            if ($state == 200)
                            {
                                   $link = $obj->url;
                                   return $link;
                            }
                            else
                            {
                                   $link = null;
                                   return $link;
                            }
                     }
                     else
                     {
                            $link = null;
                            return $link;
                     }  

              }
              catch(\Zend\Http\Exception\RuntimeException $runtimeException) 
              {
                     $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/ogmo.log');
                     $logger = new \Zend\Log\Logger();
                     $logger->addWriter($writer);
                     $logger->info($runtimeException->getMessage());
              }

       }

}